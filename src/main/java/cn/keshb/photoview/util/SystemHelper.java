package cn.keshb.photoview.util;

import javafx.stage.Screen;

public class SystemHelper {

    public static double screenWidth() {
        return Screen.getPrimary().getBounds().getWidth();
    }

    public static double screenHeight() {
        return Screen.getPrimary().getBounds().getHeight();
    }
}
