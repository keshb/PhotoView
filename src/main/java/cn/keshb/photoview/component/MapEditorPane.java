package cn.keshb.photoview.component;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class MapEditorPane extends BorderPane implements Initializable {

    @FXML
    private ToolBar toolBar;
    @FXML
    private Button rectButton;
    @FXML
    private ScrollPane scrollPane;
    //    @FXML
//    private ImageView imageView;
    @FXML
    private Canvas canvas;

    public MapEditorPane() {
        FXMLLoader fxmlLoader = new FXMLLoader(
                getClass().getResource("/component/MapEditorPane/map-editor-pane.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    private static final Map<File, Stage> stageManager = new HashMap<>(4);

    public static Optional<Stage> getInstance(File file) {
        Stage stage = stageManager.get(file);
        if (stage == null) {
            stage = new Stage();
            stageManager.put(file, stage);
            try {
                MapEditorPane mapEditorPane = new MapEditorPane();
                Image image = new Image(new FileInputStream(file));
                mapEditorPane.scrollPane.prefViewportHeightProperty().set(image.getHeight());
                mapEditorPane.scrollPane.prefViewportWidthProperty().set(image.getWidth());
                mapEditorPane.scrollPane.setBackground(new Background(
                        new BackgroundImage(image, BackgroundRepeat.NO_REPEAT
                                , BackgroundRepeat.NO_REPEAT
                                , BackgroundPosition.CENTER
                                , new BackgroundSize(image.getWidth(), image.getHeight(), false, false, false, false))));
//                mapEditorPane.canvas.setHeight(image.getHeight());
//                mapEditorPane.canvas.setWidth(image.getWidth());
                System.out.println(image.getHeight());
                System.out.println(image.getWidth());
                System.out.println(mapEditorPane.scrollPane.getPrefViewportHeight());
                System.out.println(mapEditorPane.scrollPane.getPrefViewportWidth());
                stage.setScene(new Scene(mapEditorPane));
                stage.setResizable(false);
                stage.initStyle(StageStyle.DECORATED);
                mapEditorPane.scrollPane.setOnMouseClicked(event -> {
                    System.out.println(event.getX());
                    System.out.println(event.getY());
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return Optional.of(stage);
    }
}
