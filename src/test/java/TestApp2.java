import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;


public class TestApp2 extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    Rectangle rect = new Rectangle(100, 100);

    @Override
    public void start(Stage stage) {
        rect.setFill(Color.color(0.0,0.0,0.1,0.01));
        rect.setStrokeWidth(1);
        rect.setStroke(Color.color(0,0,0.5,0.2));
        rect.strokeProperty();

        StackPane root = new StackPane();
        root.getChildren().add(rect);

        Scene scene = new Scene(root, 300, 250);

        stage.setTitle("Hello World!");
        stage.setScene(scene);
        stage.show();
    }
}
